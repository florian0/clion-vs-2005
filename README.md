# clion-vs-2005

Setup Jetbrain CLion with Visual Studio 2005

## 1. Patch Visual Studio 2005

CLion requires the environment variable `%VisualStudioVersion%` to be present. Visual Studio 2005 does not populate this environment variable, therefore CLion will only state *Not found* when simply adding Visual Studio 2005 as a toolchain.

Change this file `%VS80COMNTOOLS%vsvars32.bat` and add this line at a appropriate location.

```batch
@set VisualStudioVersion=8.0
```

## 2. Add Toolchain

Next add Visual Studio 2005 as a toolchain in CLion. Choose the root path of your Visual Studio install, e.g. `C:\Program Files (x86)\Visual Studio 8.0\`

CLion should then properly detect Visual Studio 2005. It will warn you about the version, but thats okay for now.

## 3. Add Release-Build configuration

For some reason, debug builds fail to execute with return code `0xC0000135` (STATUS_DLL_NOT_FOUND). Probably due to the fact that debug builds are linked against msvcXX**d**.dll versions, which can't be found.

A workaround is to just add the `Release`-configuration in CLion and stick to that until a better solution is available.